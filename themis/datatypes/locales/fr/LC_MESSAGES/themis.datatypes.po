msgid ""
msgstr ""
"Project-Id-Version: themis.datatypes 1.0\n"
"POT-Creation-Date: 2012-04-12 10:38+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI +ZONE\n"
"Last-Translator: Frederic Peters <fpeters@entrouvert.com>\n"
"Language-Team: French <@>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0\n"
"Language-Code: fr\n"
"Language-Name: French\n"
"Preferred-Encodings: utf-8 latin1\n"
"Domain: themis.datatypes\n"

#: ./profiles/default/types/themis.datatypes.commission.xml
msgid "A commission"
msgstr "Une commission"

#: ./profiles/default/types/themis.datatypes.contact.xml
msgid "A contact"
msgstr "Un contact"

#: ./profiles/default/types/themis.datatypes.deputy.xml
msgid "A deputy"
msgstr "Un député"

#: ./profiles/default/types/themis.datatypes.ministry.xml
msgid "A ministry"
msgstr "Un ministre"

#: ./profiles/default/types/themis.datatypes.polgroup.xml
msgid "A political group"
msgstr "Un groupe politique"

#: ./commission.py:18
#: ./deputy.py:33
#: ./ministry.py:15
msgid "Active"
msgstr "En activité"

#: ./deputy.py:44
msgid "Bio (oneline)"
msgstr "Biographie (une ligne)"

#: ./deputy.py:45
msgid "Biography"
msgstr "Biographie"

#: ./deputy.py:35
msgid "Birthdate"
msgstr "Date de naissance"

#: ./deputy.py:43
msgid "Birthplace"
msgstr "Lieu de naissance"

#: ./address.py:19
msgid "City"
msgstr "Ville"

#: ./commission.py:19
msgid "Code"
msgstr "Code"

#: ./profiles/default/types/themis.datatypes.commission.xml
msgid "Commission"
msgstr "Commission"

#: ./commission.py:34
msgid "Competence"
msgstr "Compétence"

#: ./commission.py:33
msgid "Competences"
msgstr "Compétences"

#: ./profiles/default/types/themis.datatypes.contact.xml
msgid "Contact"
msgstr "Contact"

#: ./deputy.py:51
msgid "Current Functions"
msgstr "Fonctions actuelles"

#: ./deputy.py:48
msgid "Degrees"
msgstr "Diplômes"

#: ./profiles/default/types/themis.datatypes.deputy.xml
msgid "Deputy"
msgstr "Député"

#: ./deputy.py:41
msgid "District"
msgstr "Arrondissement"

#: ./address.py:23
msgid "Email"
msgstr "Courriel"

#: ./address.py:22
msgid "Fax"
msgstr "Fax"

#: ./deputy.py:31
#: ./ministry.py:12
msgid "First Name"
msgstr "Prénom"

#: ./ministry.py:14
msgid "Function"
msgstr "Fonction"

#: ./deputy.py:32
#: ./ministry.py:13
msgid "Last Name"
msgstr "Nom"

#: ./commission.py:27
msgid "Member"
msgstr "Membre"

#: ./commission.py:26
msgid "Members"
msgstr "Membres"

#: ./commission.py:36
msgid "Ministries"
msgstr "Ministres"

#: ./commission.py:37
#: ./profiles/default/types/themis.datatypes.ministry.xml
msgid "Ministry"
msgstr "Ministre"

#: ./commission.py:17
#: ./contact.py:9
#: ./polgroup.py:9
msgid "Name"
msgstr "Nom"

#: ./deputy.py:50
msgid "Other Mandates"
msgstr "Autres mandats"

#: ./deputy.py:52
msgid "Past Functions"
msgstr "Fonctions passées"

#: ./address.py:20
msgid "Phone"
msgstr "Téléphone"

#: ./address.py:21
msgid "Phone 2"
msgstr "Téléphone (2)"

#: ./deputy.py:46
msgid "Picture"
msgstr "Photo"

#: ./deputy.py:37
#: ./profiles/default/types/themis.datatypes.polgroup.xml
msgid "Political Group"
msgstr "Groupe politique"

#: ./commission.py:21
msgid "President"
msgstr "Président"

#: ./deputy.py:54
msgid "Private Address"
msgstr "Adresse privée"

#: ./deputy.py:49
msgid "Profession"
msgstr "Profession"

#: ./deputy.py:40
msgid "Seat Number"
msgstr "Numéro du siège"

#: ./commission.py:39
msgid "Secretariat"
msgstr "Secrétariat"

#: ./deputy.py:34
msgid "Sex"
msgstr "Sexe"

#: ./address.py:17
msgid "Street"
msgstr "Rue"

#: ./commission.py:30
msgid "Substitute"
msgstr "Suppléant"

#: ./commission.py:29
msgid "Substitutes"
msgstr "Suppléants"

#: ./address.py:16
msgid "Title"
msgstr "Titre"

#: ./commission.py:24
msgid "Vice-President"
msgstr "Vice-président"

#: ./commission.py:23
msgid "Vice-Presidents"
msgstr "Vice-présidents"

#: ./deputy.py:47
msgid "Website"
msgstr "Site web"

#: ./deputy.py:55
msgid "Work Address"
msgstr "Adresse professionnelle"

#: ./deputy.py:56
msgid "Work Address (2)"
msgstr "Adresse professionnelle (2)"

#: ./address.py:18
msgid "Zip"
msgstr "Code postal"

