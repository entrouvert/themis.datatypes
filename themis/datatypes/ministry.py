from five import grok
from zope import schema
from zope.interface import implements
from z3c.relationfield.interfaces import IHasRelations

from plone.directives import form, dexterity
from plone.dexterity.content import Item

from themis.datatypes.interfaces import MessageFactory as _

class IMinistry(form.Schema):
    firstname = schema.TextLine(title=_(u'First Name'))
    lastname = schema.TextLine(title=_(u'Last Name'))
    function = schema.TextLine(title=_(u'Function'))
    active = schema.Bool(title=_(u'Active'), default=True)

class Ministry(Item):
    implements(IMinistry, IHasRelations)

    @property
    def title(self):
        return self.firstname + ' ' + self.lastname

    def setTitle(self, value):
        pass

