import datetime

from five import grok
from zope import schema
from zope.schema import Field
from zope.schema.interfaces import IField, IFromUnicode
from zope.interface import implements, implementer
from zope.component import adapts, adapter, provideAdapter
from z3c.relationfield.interfaces import IHasRelations

from z3c.relationfield.schema import RelationChoice
from plone.formwidget.contenttree import ObjPathSourceBinder
from plone.dexterity.content import Item
from plone.app.textfield import RichText
from plone.namedfile.field import NamedBlobImage

from plone.app.content.interfaces import INameFromTitle
from plone.directives import form, dexterity

from z3c.form.browser.text import TextWidget
from z3c.form.interfaces import IFormLayer, IFieldWidget
from z3c.form.widget import FieldWidget

from themis.datatypes.interfaces import MessageFactory as _

from themis.datatypes.polgroup import IPolGroup

from address import Address

class IDeputy(form.Schema):
    firstname = schema.TextLine(title=_(u'First Name'))
    lastname = schema.TextLine(title=_(u'Last Name'))
    active = schema.Bool(title=_(u'Active'), default=True)
    sex = schema.Choice(title=_(u'Sex'), values=('M', 'F'), required=False)
    birthdate = schema.Date(title=_(u'Birthdate'), required=False)
    polgroup = RelationChoice(
                title=_(u"Political Group"),
                source=ObjPathSourceBinder(object_provides=IPolGroup.__identifier__),
                required=False)
    seat_number = schema.TextLine(title=_(u'Seat Number'), required=False)
    district = schema.TextLine(title=_(u'District'), required=False)

    birthplace = schema.TextLine(title=_(u'Birthplace'), required=False)
    bio = schema.Text(title=_(u'Bio (oneline)'), required=False)
    biography = RichText(title=_(u'Biography'), required=False)
    picture = NamedBlobImage(title=_(u'Picture'), required=False)
    website = schema.TextLine(title=_(u'Website'), required=False)
    degrees = schema.Text(title=_(u'Degrees'), required=False)
    profession = schema.Text(title=_(u'Profession'), required=False)
    mandates = schema.Text(title=_(u'Other Mandates'), required=False)
    current_functions = RichText(title=_(u'Current Functions'), required=False)
    past_functions = RichText(title=_(u'Past Functions'), required=False)

    private_address = Address(title=_(u'Private Address'), required=False)
    work_address = Address(title=_(u'Work Address'), required=False)
    work_address_2 = Address(title=_(u'Work Address (2)'), required=False)


class INameFromPersonNames(INameFromTitle):
    def title():
        '''Return a processed title'''

class NameFromPersonNames(object):
    implements(INameFromPersonNames)

    def __init__(self, context):
        self.context = context

    @property
    def title(self):
        return u'%s %s' % (self.context.firstname, self.context.lastname)

class Deputy(Item):
    implements(IDeputy, IHasRelations)

    @property
    def title(self):
        return u'%s %s' % (self.firstname, self.lastname)

    def Title(self):
        return self.title

    def setTitle(self, value):
        pass

    @property
    def agerange(self):
        if not self.birthdate:
            return '?'
        today = datetime.datetime.today()
        age = (today.year - self.birthdate.year)
        if today.month < self.birthdate.month or \
                (today.month == self.birthdate.month and
                 today.day < self.birthdate.day):
            age -= 1
        if age < 30:
            return 'm30'
        elif age < 40:
            return 'd30-40'
        elif age < 50:
            return 'd40-50'
        elif age < 60:
            return 'd50-60'
        elif age < 70:
            return 'd60-70'
        else:
            return 'p70'

    def formatted_birthdate(self):
        if not self.birthdate:
            return ''
        return self.birthdate.strftime('%d/%m/%Y')

    def sortname(self):
       from plone.i18n.normalizer.fr import normalizer
       # | replacement is an hack to get spaces to sort after letters
       return normalizer.normalize('%s %s' % (
                               self.lastname.replace(' ', '|'),
                               self.firstname.replace(' ', '|'))).lower()
