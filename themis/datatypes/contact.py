from five import grok
from zope import schema

from plone.directives import form, dexterity

from themis.datatypes.interfaces import MessageFactory as _

class IContact(form.Schema):
    title = schema.TextLine(title=_(u'Name'))

