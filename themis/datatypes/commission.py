from five import grok
from zope import schema
from zope.interface import implements

from plone.directives import form, dexterity

from plone.dexterity.content import Item
from z3c.relationfield.schema import RelationChoice, RelationList
from z3c.relationfield.interfaces import IHasRelations
from z3c.relationfield import RelationValue
from plone.formwidget.contenttree import ObjPathSourceBinder
from plone.app.textfield import RichText

from themis.datatypes.interfaces import MessageFactory as _

class ICommission(form.Schema):
    title = schema.TextLine(title=_(u'Name'))
    active = schema.Bool(title=_(u'Active'), default=True)
    code = schema.TextLine(title=_(u'Code'), required=False)

    president = RelationChoice(title=_('President'), required=False,
                               source=ObjPathSourceBinder())
    vicepresidents = RelationList(title=_(u'Vice-Presidents'), default=[], required=False,
                           value_type=RelationChoice(title=_(u'Vice-President'),
                                                     source=ObjPathSourceBinder()))
    members = RelationList(title=_(u'Members'), default=[], required=False,
                           value_type=RelationChoice(title=_(u'Member'),
                                                     source=ObjPathSourceBinder()))
    substitutes = RelationList(title=_(u'Substitutes'), default=[], required=False,
                           value_type=RelationChoice(title=_(u'Substitute'),
                                                     source=ObjPathSourceBinder()))

    competences = schema.List(title=_(u'Competences'), default=[], required=False,
                           value_type=schema.TextLine(title=_(u'Competence')))

    ministries = RelationList(title=_(u'Ministries'), default=[], required=False,
                           value_type=RelationChoice(title=_(u'Ministry'),
                                                     source=ObjPathSourceBinder()))
    secretariat = RichText(title=_(u'Secretariat'), required=False)

class Commission(Item):
    implements(ICommission, IHasRelations)
