from zope.interface import Interface
from zope import schema
from zope.schema import Object, Field
from zope.schema.interfaces import IObject, IField, IFromUnicode
from zope.interface import implements, implementer
from zope.component import adapts, adapter, provideAdapter

from z3c.form.interfaces import IFormLayer, IFieldWidget, NOVALUE
from z3c.form.widget import Widget, FieldWidget
from z3c.form.interfaces import IWidget
from z3c.form.converter import BaseDataConverter

from themis.datatypes.interfaces import MessageFactory as _

class IAddress(IField):
    title = schema.TextLine(title=_(u'Title'), required=False)
    street = schema.TextLine(title=_(u'Street'), required=False)
    zipcode = schema.TextLine(title=_(u'Zip'), required=False)
    city = schema.TextLine(title=_(u'City'), required=False)
    phone1 = schema.TextLine(title=_(u'Phone'), required=False)
    phone2 = schema.TextLine(title=_(u'Phone 2'), required=False)
    fax = schema.TextLine(title=_(u'Fax'), required=False)
    email = schema.TextLine(title=_(u'Email'), required=False)

class Address(Field):
    implements(IAddress, IFromUnicode)

    title = None
    street = None
    zipcode = None
    city = None
    phone1 = None
    phone2 = None
    fax = None
    email = None

    def as_dict(self):
        return {
                'title': self.title,
                'street': self.street,
                'zipcode': self.zipcode,
                'city': self.city,
                'phone1': self.phone1,
                'phone2': self.phone2,
                'fax': self.fax,
                'email': self.email,
               }

    def from_dict(cls, d):
        o = cls()
        if type(d) is list:
            d = d[0] # wtf?
        for attr in ('title', 'street', 'zipcode', 'city', 'phone1', 'phone2',
                     'fax', 'email'):
            setattr(o, attr, d.get(attr))
        return o
    from_dict = classmethod(from_dict)

    @property
    def email_str(self):
        if not self.email:
            return ''
        return self.email.replace('@', ' arobase ').replace('.', ' point ')

class IAddressWidget(IWidget):
    pass

class AddressWidget(Widget):
    implements(IAddressWidget)

    def update(self):
        super(AddressWidget, self).update()

    def extract(self, default=NOVALUE):
        if not (self.name + '.street') in self.request.form:
            return NOVALUE
        address = Address()
        has_value = None
        for attr in ('title', 'street', 'zipcode', 'city', 'phone1', 'phone2',
                     'fax', 'email'):
            setattr(address, attr, self.request.get(self.name + '.' + attr))
            has_value = has_value or getattr(address, attr)
        if not has_value:
            return NOVALUE
        return address


@adapter(IAddress, IFormLayer)
@implementer(IFieldWidget)
def AddressFieldWidget(field, request):
    """IFieldWidget factory for Address."""
    return FieldWidget(field, AddressWidget(request))


class AddressConverter(BaseDataConverter):
    adapts(IAddress, IAddressWidget)

    def toWidgetValue(self, value):
        if value is None:
            return Address()
        return value

    def toFieldValue(self, value):
        return value

