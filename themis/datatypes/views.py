import json

from Products.Five import BrowserView

class DeputyJson(BrowserView):
    def __call__(self):
        d = {
           'id': self.context.id,
           'firstname': self.context.firstname,
           'lastname': self.context.lastname,
           'active': self.context.active,
           'sex': self.context.sex,
           'district': self.context.district,
           'birthplace': self.context.birthplace,
           'bio': self.context.bio,
           'website': self.context.website,
           'degrees': self.context.degrees,
           'mandates': self.context.mandates,
           'profession': self.context.profession,
           'seat_number': self.context.seat_number,
        }
        if self.context.polgroup:
           d['polgroup'] = self.context.polgroup.to_object.title
        if self.context.picture:
           d['picture'] = self.context.absolute_url() + '/photo'
        if self.context.birthdate:
           d['birthdate'] = self.context.birthdate.strftime('%Y-%m-%d')
        if self.context.private_address:
            d['private_address'] = self.context.private_address.as_dict(),
        if self.context.work_address:
            d['work_address'] = self.context.work_address.as_dict(),
        if self.context.work_address_2:
            d['work_address_2'] = self.context.work_address_2.as_dict()
        if self.context.current_functions and self.context.current_functions.raw:
            d['current_functions'] = self.context.current_functions.raw
        if self.context.past_functions and self.context.past_functions.raw:
            d['past_functions'] = self.context.past_functions.raw
        if self.context.biography and self.context.biography.raw:
            d['biography'] = self.context.biography.raw

        self.request.response.setHeader('Content-type', 'application/json')
        return json.dumps(d)

class DeputyPhoto(BrowserView):
    def __call__(self):
        self.request.response.setHeader('Content-type',
                       self.context.picture.contentType)
        return self.context.picture.data

class CommissionJson(BrowserView):
    def __call__(self):
        import themis.importexport.utils
        d = {'id': self.context.id}
        for k in ('title', 'active', 'president', 'vicepresidents', 'members',
                  'substitutes', 'competences', 'ministries'):
            if not hasattr(self.context, k):
                continue
            v = getattr(self.context, k)
            d[k] = themis.importexport.utils.convert(v)

        self.request.response.setHeader('Content-type', 'application/json')
        return json.dumps(d)

